#![no_std]
#![feature(negative_impls)]
#![feature(asm)]

mod io;
mod sync;
mod volatile;

use core::panic::PanicInfo;

use io::serial::SerialLogger;
use log::{debug, Level};

#[panic_handler]
fn panic(_info: &PanicInfo) -> ! {
    kprintln!("{}", _info);
    loop {}
}

static LOGGER_LEVEL: Level = Level::Debug;
static SERIAL_LOGGER: SerialLogger = SerialLogger::new(LOGGER_LEVEL);

fn logger_init() {
    log::set_logger(&SERIAL_LOGGER)
        .map(|()| log::set_max_level(LOGGER_LEVEL.to_level_filter()))
        .unwrap();
}

#[no_mangle]
pub extern "C" fn kernel_main() {
    logger_init();
    kprintln!("Hello, World!");
    kprintln!("Welcome to Simple OS");
    debug!("This is to print to the console!");
}
