#[repr(transparent)]
pub struct Volatile<T> {
    val: T,
}

impl<T> Volatile<T> {
    pub fn write(&mut self, val: T) {
        unsafe {
            core::ptr::write_volatile(&mut self.val as *mut T, val);
        }
    }

    pub fn read(&self) -> T {
        unsafe { core::ptr::read_volatile(&self.val as *const T) }
    }
}
