use core::{
    cell::UnsafeCell,
    fmt::{self, Debug, Display, Formatter},
    ops::{Deref, DerefMut},
    sync::atomic::{AtomicBool, Ordering},
};

pub struct SpinLock<T> {
    val: UnsafeCell<T>,
    locked: AtomicBool,
}

const LOCKED: bool = true;
const UNLOCKED: bool = false;

// SpinLock doesn't implement "poisoning".
// The assumption is that there are no internal panics.
impl<T> SpinLock<T> {
    pub fn new(val: T) -> Self {
        Self {
            val: UnsafeCell::new(val),
            locked: AtomicBool::new(UNLOCKED),
        }
    }

    pub fn lock(&self) -> SpinLockGuard<'_, T> {
        while self
            .locked
            .compare_exchange_weak(UNLOCKED, LOCKED, Ordering::Acquire, Ordering::Relaxed)
            .is_err()
        {
            while self.locked.load(Ordering::Relaxed) == LOCKED {}
        }
        // Safety: We now hold the lock, so it safe to get a (mutable) reference to T.
        SpinLockGuard {
            val: unsafe { &mut *self.val.get() },
            locked: &self.locked,
        }
    }
}

// Safety: Because we synchronized things properly
unsafe impl<T: Send> Send for SpinLock<T> {}
unsafe impl<T: Send> Sync for SpinLock<T> {}

pub struct SpinLockGuard<'a, T: ?Sized + 'a> {
    val: &'a mut T,
    locked: &'a AtomicBool,
}

impl<T: ?Sized> Drop for SpinLockGuard<'_, T> {
    fn drop(&mut self) {
        self.locked.store(UNLOCKED, Ordering::Release);
    }
}

impl<T: ?Sized> Deref for SpinLockGuard<'_, T> {
    type Target = T;

    fn deref(&self) -> &Self::Target {
        self.val
    }
}

impl<T: ?Sized> DerefMut for SpinLockGuard<'_, T> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        self.val
    }
}

impl<T: ?Sized + Debug> Debug for SpinLockGuard<'_, T> {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        write!(f, "{:?}", self.val)
    }
}

impl<T: ?Sized + Display> Display for SpinLockGuard<'_, T> {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        write!(f, "{}", self.val)
    }
}

impl<T: ?Sized> !Send for SpinLockGuard<'_, T> {}

unsafe impl<T: ?Sized + Sync> Sync for SpinLockGuard<'_, T> {}
