mod lock;

pub use lock::{SpinLock, SpinLockGuard};
