use crate::sync::SpinLock;
use core::fmt;
use lazy_static::lazy_static;

struct SerialPort(u16);

impl SerialPort {
    fn write_byte(&self, byte: u8) {
        unsafe { asm!("out dx, al", in("dx") self.0, in("al") byte) }
    }

    fn read_byte(&self) -> u8 {
        let x: u8;
        unsafe { asm!("in al, dx", in("dx") self.0, out("al") x) }
        x
    }
}

pub struct SerialDevice {
    data: SerialPort,
    intr_en: SerialPort,
    fifo_ctrl: SerialPort,
    line_ctrl: SerialPort,
    modem_ctrl: SerialPort,
    line_stat: SerialPort,
}

impl SerialDevice {
    fn new(port: u16) -> Self {
        Self {
            data: SerialPort(port),
            intr_en: SerialPort(port + 1),
            fifo_ctrl: SerialPort(port + 2),
            line_ctrl: SerialPort(port + 3),
            modem_ctrl: SerialPort(port + 4),
            line_stat: SerialPort(port + 5),
        }
    }

    fn init(&self) {
        // Disable interrupts
        self.intr_en.write_byte(0x00);
        // Enable DLAB (set baud rate divisor)
        self.line_ctrl.write_byte(0x80);
        // Set divisor to 3 (lo byte) 38400 baud
        self.data.write_byte(0x03);
        // Set divisor to 3 (hi byte) 38400 baud
        self.intr_en.write_byte(0x00);
        // 8 bits, no parity, one stop bit
        self.line_ctrl.write_byte(0x03);
        // Enable FIFO, clear them, with 14-byte threshold
        self.fifo_ctrl.write_byte(0xC7);
        // IRQs enabled, RTS/DSR set
        self.modem_ctrl.write_byte(0x0B);

        // Set in loopback mode, test the serial chip
        self.modem_ctrl.write_byte(0x1E);
        // Test serial chip
        self.data.write_byte(0xAE);
        if self.data.read_byte() != 0xAE {
            panic!("failed to setup serial device");
        }

        // Set to normal mode
        self.modem_ctrl.write_byte(0x0F);
    }

    fn write_byte(&self, byte: u8) {
        while self.line_stat.read_byte() & 0x20 == 0 {}
        self.data.write_byte(byte);
    }
}

impl fmt::Write for SerialDevice {
    fn write_str(&mut self, s: &str) -> fmt::Result {
        for byte in s.as_bytes() {
            self.write_byte(*byte);
        }
        Ok(())
    }
}

lazy_static! {
    pub static ref WRITER: SpinLock<SerialDevice> = {
        let dev = SerialDevice::new(0x3F8);
        dev.init();
        SpinLock::new(dev)
    };
}

#[macro_export]
macro_rules! sprint {
    ($($arg:tt)*) => ($crate::io::serial::_print(format_args!($($arg)*)));
}

#[macro_export]
macro_rules! sprintln {
    () => ($crate::kprint!("\n"));
    ($($arg:tt)*) => ($crate::sprint!("{}\n", format_args!($($arg)*)));
}

#[doc(hidden)]
pub fn _print(args: fmt::Arguments) {
    use core::fmt::Write;
    WRITER.lock().write_fmt(args).unwrap();
}

pub struct SerialLogger(log::Level);

impl SerialLogger {
    pub const fn new(level: log::Level) -> Self {
        Self(level)
    }
}

impl log::Log for SerialLogger {
    fn enabled(&self, metadata: &log::Metadata) -> bool {
        self.0 <= metadata.level()
    }

    fn log(&self, record: &log::Record) {
        if self.enabled(record.metadata()) {
            sprintln!(
                "{}:{} -- {}",
                record.level(),
                record.target(),
                record.args()
            );
        }
    }

    fn flush(&self) {}
}
