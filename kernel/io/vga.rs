use crate::sync::SpinLock;
use crate::volatile::Volatile;
use core::fmt;
use lazy_static::lazy_static;

#[allow(dead_code)]
#[repr(u8)]
#[derive(Copy, Clone, PartialEq, Eq)]
enum Color {
    Black,
    Blue,
    Green,
    Cyan,
    Red,
    Magenta,
    Brown,
    LightGray,
    DarkGray,
    LightBlue,
    LightGreen,
    LightCyan,
    LightRed,
    LightMagenta,
    LightBrown,
    White,
}

#[repr(transparent)]
#[derive(Copy, Clone, PartialEq, Eq)]
struct ColorCode(u8);

impl ColorCode {
    fn new(fg: Color, bg: Color) -> ColorCode {
        ColorCode(fg as u8 | (bg as u8) << 4)
    }
}

#[repr(C)]
#[derive(Copy, Clone, PartialEq, Eq)]
struct Entry {
    ch: u8,
    color: ColorCode,
}

const BUFFER_WIDTH: usize = 80;
const BUFFER_HEIGHT: usize = 25;

#[repr(transparent)]
struct TextBuffer {
    entries: [[Volatile<Entry>; BUFFER_WIDTH]; BUFFER_HEIGHT],
}

pub struct Writer {
    row: usize,
    column: usize,
    color: ColorCode,
    buf: &'static mut TextBuffer,
}

impl Writer {
    fn new(color: ColorCode, buf: &'static mut TextBuffer) -> Self {
        let mut wt = Writer {
            row: 0,
            column: 0,
            color,
            buf,
        };
        wt.clear();
        wt
    }

    fn write_byte(&mut self, byte: u8) {
        if byte == b'\n' {
            self.write_newline();
        } else {
            if self.column >= BUFFER_WIDTH {
                self.column = 0;
                self.row += 1;
            }
            if self.row >= BUFFER_HEIGHT {
                for i in 0..BUFFER_HEIGHT - 1 {
                    for j in 0..BUFFER_WIDTH {
                        self.buf.entries[i][j].write(self.buf.entries[i + 1][j].read());
                    }
                }
                self.row = BUFFER_HEIGHT - 1;
            }

            self.write_byte_at(byte, self.row, self.column);
            self.column += 1;
        }
    }

    fn write_byte_at(&mut self, byte: u8, row: usize, column: usize) {
        self.buf.entries[row][column].write(Entry {
            ch: byte,
            color: self.color,
        });
    }

    fn write_string(&mut self, s: &str) {
        for byte in s.bytes() {
            match byte {
                0x20..=0x7e | b'\n' => self.write_byte(byte),
                _ => self.write_byte(0xfe),
            }
        }
    }

    fn write_newline(&mut self) {
        self.row += 1;
        self.column = 0;
    }

    fn clear(&mut self) {
        for i in 0..BUFFER_HEIGHT {
            for j in 0..BUFFER_WIDTH {
                self.write_byte_at(b' ', i, j);
            }
        }
    }
}

impl fmt::Write for Writer {
    fn write_str(&mut self, s: &str) -> fmt::Result {
        self.write_string(s);
        Ok(())
    }
}

lazy_static! {
    pub static ref WRITER: SpinLock<Writer> = SpinLock::new(Writer::new(
        ColorCode::new(Color::LightBrown, Color::Black),
        unsafe { &mut *(0xb8000 as *mut TextBuffer) },
    ));
}

#[macro_export]
macro_rules! kprint {
    ($($arg:tt)*) => ($crate::io::vga::_print(format_args!($($arg)*)));
}

#[macro_export]
macro_rules! kprintln {
    () => ($crate::kprint!("\n"));
    ($($arg:tt)*) => ($crate::kprint!("{}\n", format_args!($($arg)*)));
}

#[doc(hidden)]
pub fn _print(args: fmt::Arguments) {
    use core::fmt::Write;
    WRITER.lock().write_fmt(args).unwrap();
}
