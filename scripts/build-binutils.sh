#!/bin/bash

# This script cross-compiles binutils (so that we have `as` and `ld`)

set -e

BUILD_DIR="$PWD/build/binutils"
SRC_DIR="$PWD/tools/binutils-2.37"
PREFIX="$PWD/cross"
TARGET=x86_64-elf

[ -e "$BUILD_DIR" ] && rm -rf "$BUILD_DIR"
mkdir -p "$BUILD_DIR"
[ -e "$PREFIX" ] || mkdir "$PREFIX"

cd "$BUILD_DIR"
${SRC_DIR}/configure --target=$TARGET --prefix="$PREFIX" \
    --with-sysroot --disable-nls --disable-werror \
    --disable-gdb --disable-libdecnumber --disable-readline --disable-sim
make -j8
make install

exit 0