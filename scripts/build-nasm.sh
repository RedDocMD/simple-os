#!/bin/bash

# This script cross-compiles nasm.

set -e

BUILD_DIR="$PWD/build/nasm"
SRC_DIR="$PWD/tools/nasm-2.15.05"
PREFIX="$PWD/cross"
TARGET=x86_64-elf

[ -e "$BUILD_DIR" ] && rm -rf "$BUILD_DIR"
mkdir -p "$BUILD_DIR"
[ -e "$PREFIX" ] || mkdir "$PREFIX"


cd "$BUILD_DIR"
${SRC_DIR}/configure --target=$TARGET --prefix="$PREFIX" \
    --disable-werror --disable-gdb
make -j8
make install

exit 0