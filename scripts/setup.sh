#!/bin/bash

# This is the overall setup script
# Run this the FIRST time you clone the repo.

set -e

SCRIPTS_DIR=${PWD}/scripts

${SCRIPTS_DIR}/build-nasm.sh
${SCRIPTS_DIR}/rust-setup.sh

exit 0