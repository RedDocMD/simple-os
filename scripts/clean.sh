#!/bin/bash

# This script cleans all the build files from the build directory

set -e

BUILD_DIR=${PWD}/build
OS_NAME=simple-os

find $BUILD_DIR -name '*.o' | xargs -i rm {}
rm -f ${BUILD_DIR}/${OS_NAME}.bin
rm -f ${BUILD_DIR}/${OS_NAME}.iso
rm -rf ${BUILD_DIR}/isofiles
cargo clean --target-dir ${BUILD_DIR}/kernel