#!/bin/bash

# This script confirms the availability of the Rust toolchain
# and sets it up appropriately.

set -e

if ! command -v rustup &> /dev/null; then
    echo "rustup not found. Please install rustup."
    exit 1
fi

if ! command -v cargo &> /dev/null; then
    echo "please install the Rust toolchain, including cargo."
    exit 1
fi

rustup override set nightly

exit 0