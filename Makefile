ARCH ?= x86_64-elf
AS := cross/bin/nasm
LD := ld.lld

kernel := build/kernel/simple-os/release/libkernel.a

run: build/simple-os.iso
	qemu-system-x86_64 -serial stdio -cdrom $< -s

debug: build/simple-os.iso
	qemu-system-x86_64 -serial stdio -cdrom $< -s -S

build/simple-os.iso: build/simple-os.bin boot/grub.cfg
	mkdir -p build/isofiles/boot/grub
	cp boot/grub.cfg build/isofiles/boot/grub/grub.cfg
	cp build/simple-os.bin build/isofiles/boot/simple-os.bin
	grub-mkrescue -o build/simple-os.iso build/isofiles

build/simple-os.bin: build/boot.o build/long_mode_init.o build/multiboot.o kernel
	$(LD) -n -T boot/linker.ld -o $@ \
		build/boot.o \
		build/long_mode_init.o \
		build/multiboot.o \
		$(kernel)

kernel:
	cargo build --release

build/boot.o: boot/boot.asm
	$(AS) -f elf64 -o $@ $<
build/long_mode_init.o: boot/long_mode_init.asm
	$(AS) -f elf64 -o $@ $<
build/multiboot.o: boot/multiboot.asm
	$(AS) -f elf64 -o $@ $<

clean:
	./scripts/clean.sh

.PHONY: clean run kernel
