; Make the multiboot2 header
section .multiboot
header_start:
    align 4
    dd 0xE85250D6 ; Magic number
    dd 0 ; Arch
    dd (header_end - header_start) ; Length
    ; Checksum
    dd 0x100000000 - (0xE85250D6 + 0 + (header_end - header_start))
    ; End tag
    align 8
    dw 0
    dw 0
    dd 8
header_end: